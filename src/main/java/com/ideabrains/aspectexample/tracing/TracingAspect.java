package com.ideabrains.aspectexample.tracing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by kamauwamatu
 * Project aspect-example
 * User: kamauwamatu
 * Date: 2019-07-07
 * Time: 12:59
 */
@Aspect
@Component
public class TracingAspect {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    ObjectMapper objectMapper = new ObjectMapper();

    @Pointcut("execution(* com.ideabrains.*.controllers.*.*(..))")
    private void forAllControllers() {
    }


    @Pointcut("execution(* com.ideabrains.*.services.*.*(..))")
    private void forAllServices() {
    }

    @Pointcut("forAllControllers() || forAllServices()")
    private void forApplication() {

    }

    @Before("forApplication()")
    public void logException(JoinPoint joinPoint) {

        String methodName = joinPoint.getSignature().toShortString();
        logger.info("Before Advice Aspect called!:: {} ", methodName);
    }

    @AfterReturning(pointcut = "forApplication()", returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {
        String methodName = joinPoint.getSignature().toShortString();
        logger.info("Info: in @AfterReturning Advice returned : from method :: {} ", methodName);
        logger.info("Info: in @AfterReturning Advice returned: from method :: {} ", result);

    }


    @Around("forApplication()")
    public Object employeeAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) throws JsonProcessingException {
        String methodName = proceedingJoinPoint.getSignature().toShortString();
        logger.info("Info: Processing request : from method :: {} ", methodName);
        Object value = null;
        try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            logger.error("Error : in Exception : from method :: {} and Exception Message : {} ", methodName, e.getMessage());
            return null;
        }
        logger.info(" Returning Response : from method ::  {}  with response :: {} ", methodName, objectMapper.writeValueAsString(value));
        return value;
    }


}
