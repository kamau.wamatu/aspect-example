package com.ideabrains.aspectexample.controllers;

import com.ideabrains.aspectexample.dtos.RequestArray;
import com.ideabrains.aspectexample.responses.ResponseArray;
import com.ideabrains.aspectexample.services.IndexServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by kamauwamatu
 * Project aspect-example
 * User: kamauwamatu
 * Date: 2019-07-07
 * Time: 12:54
 */
@RestController
public class IndexController {

    @Autowired
    IndexServiceImpl indexService;


    @GetMapping("/index/{name}")
    public String returnString(@PathVariable String name) {
        return indexService.getName(name);
    }


    @PostMapping("/index/")
    public ResponseEntity<?> processJsonRequest(@RequestBody @Valid RequestArray requestArray) {

        ResponseArray responseArray = indexService.processRegistration(requestArray);

        return new ResponseEntity<>(responseArray, HttpStatus.OK);
    }


}
