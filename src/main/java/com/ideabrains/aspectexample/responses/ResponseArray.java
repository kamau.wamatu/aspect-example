package com.ideabrains.aspectexample.responses;

import lombok.Data;

/**
 * Created by kamauwamatu
 * Project aspect-example
 * User: kamauwamatu
 * Date: 2019-07-09
 * Time: 00:17
 */
@Data
public class ResponseArray {

    private int statusCode;
    private String statusMessage;
    private Object result;


}
