package com.ideabrains.aspectexample.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Created by kamauwamatu
 * Project aspect-example
 * User: kamauwamatu
 * Date: 2019-07-09
 * Time: 00:14
 */
@Data
public class RequestArray {

    @NotBlank(message = "First Name is a required field")
    private String firstName;
    @NotBlank(message = "Second Name is a required field")
    private String secondName;
}
