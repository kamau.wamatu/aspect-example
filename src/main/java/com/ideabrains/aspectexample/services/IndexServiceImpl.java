package com.ideabrains.aspectexample.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ideabrains.aspectexample.dtos.RequestArray;
import com.ideabrains.aspectexample.responses.ResponseArray;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Created by kamauwamatu
 * Project aspect-example
 * User: kamauwamatu
 * Date: 2019-07-07
 * Time: 12:57
 */
@Service
public class IndexServiceImpl {
    ObjectMapper objectMapper = new ObjectMapper();

    public String getName(String name) {
        return "Hey. Call me " + name;
    }

    public ResponseArray processRegistration(RequestArray requestArray) {

        ResponseArray responseArray = new ResponseArray();
        responseArray.setStatusCode(HttpStatus.OK.value());
        responseArray.setStatusMessage("Success");
        responseArray.setResult(requestArray);

        return responseArray;

    }
}
